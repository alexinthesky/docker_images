#!/bin/sh
set -x
dir=${PWD}


#for j in `find .|grep Dockerfile`;do
#  echo $j| awk -F'Dockerfile' '{print $1}'| sed 's|/||g'| sed 's/\.//g'
#done
#exit

for i in `ls -d */`;do
  cd ${dir}/$i
  image=`echo $i | sed 's/\///'`
  docker build  . -t alexinthesky/` echo $image | awk '{print tolower($0)}'`
  docker push alexinthesky/` echo $image | awk '{print tolower($0)}'`
done

