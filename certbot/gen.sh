#!/bin/bash

docker run -ti --rm -v ${PWD}/cloudflare.ini:/cloudflare.ini -v ${PWD}/letsencrypt:/etc/letsencrypt alexinthesky/certbot certonly -d "$1" --preferred-challenges dns --dns-cloudflare --agree-tos -m $2 --dns-cloudflare --dns-cloudflare-credentials /cloudflare.ini

